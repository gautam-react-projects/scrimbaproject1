import React from "react";
import "./Navbar.css";
export default function NavBar() {
  return (
    <div className="nav">
      <div className="logoDiv">
        <img className="logo" src="./Group.png" alt="logo"></img>
        <h2>ReactFacts</h2>
      </div>
      <div className="title">
        <h3>React course-Project 1</h3>
      </div>
    </div>
  );
}
