import React from "react";
import "./Main.css";

export default function MainComponent() {
  return (
    <div className="main">
      <h1>Fun facts about React</h1>
      <div className="bgdiv">
        <ul className="list">
          <li>was first released in 2013</li>
          <li>was originally created by jordan walke</li>
          <li>Has well over 100k stars on Github</li>
          <li>Is maintained by Facebook</li>
          <li>Powers thousands of enterprise apps, including mobile apps</li>
        </ul>

        <img src="./reactjs-icon 2.png"></img>
      </div>
    </div>
  );
}
